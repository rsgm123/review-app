import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import Axios from "axios";

import App from './App';
import {setReviews} from "./redux/Actions";
import reducer from "./redux/Reducers";

import * as serviceWorker from './serviceWorker';


const store = createStore(reducer);

// We'll load this on page load, should be moved to app on componentDidMount for testing purposes
Axios.get('/reviews.json').then(response => store.dispatch(setReviews(response.data))).catch(console.error);


ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
