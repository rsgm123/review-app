import React from 'react';
import {connect} from 'react-redux'
import Grid from "@material-ui/core/Grid";
import ReviewCard from "../components/ReviewCard";

export function Reviews({reviews}) {
  return (
      <Grid container spacing={3} role="list">
        {reviews.map(review =>
            <Grid item xs={12} md={6} lg={3} key={review.id}>
              <ReviewCard review={review} listCard={true}/>
            </Grid>
        )}
      </Grid>
  );
}

const mapStateToProps = (state) => {
  return {
    reviews: state.reviews,
  }
};

export default connect(mapStateToProps)(Reviews);
