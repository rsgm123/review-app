import React from 'react';
import {connect} from "react-redux";
import ReviewCard from "../components/ReviewCard";
import CommentCard from "../components/CommentCard";

export function ReviewDetail({review}) {
  return (
      <div>
        <ReviewCard review={review}/>
        <CommentCard reviewId={review.id}/>
      </div>
  );
}

const mapStateToProps = (state) => {
  return {
    review: state.reviews.find(review => review.id === state.view)
  }
};

export default connect(mapStateToProps)(ReviewDetail);
