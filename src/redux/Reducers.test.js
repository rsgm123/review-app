import reducer from './Reducers'
import {ADD_REVIEW_COMMENT, ALL_REVIEWS_VIEW, CHANGE_VIEW, SET_REVIEWS} from "./Actions";


const INITIAL_STATE = {
  view: ALL_REVIEWS_VIEW,
  reviews: [],
  reviewComments: {},
};

describe('Root reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(INITIAL_STATE)
  })
});


describe('View reducer', () => {
  it('should handle CHANGE_VIEW', () => {
    const view = '5d707203b65083001e956f0a';
    expect(reducer(INITIAL_STATE, {
      type: CHANGE_VIEW,
      view,
    })).toEqual({
      view,
      reviews: [],
      reviewComments: {},
    });
  });
});


describe('Reviews reducer', () => {
  it('should handle SET_REVIEWS', () => {
    const reviews = [{
      "id": "5d707203b65083001e956f0a",
      "author": "Weeks Duran",
      "place": "Big Johns Burgers",
      "published_at": "Thu Jun 27 1974 11:46:39 GMT-0500 (Central Daylight Time)",
      "rating": 5,
      "content": "Ipsum mollit anim pariatur eu qui velit Lorem ea enim excepteur ut fugiat fugiat esse. Incididunt consectetur deserunt pariatur magna sit dolore voluptate. Minim cupidatat fugiat magna quis consectetur esse id esse adipisicing anim velit. Cillum mollit et nisi ex occaecat labore enim nulla cupidatat. Occaecat Lorem officia est sit enim amet commodo sunt occaecat reprehenderit Lorem culpa. Aute anim ullamco voluptate incididunt incididunt excepteur in irure.\r\n"
    }];

    expect(reducer(INITIAL_STATE, {
      type: SET_REVIEWS,
      reviews,
    })).toEqual({
      view: ALL_REVIEWS_VIEW,
      reviews,
      reviewComments: {},
    });
  });
});


describe('ReviewComment reducer', () => {
  const commentA = {id: '5d707203b93029c4fd555abd', comment: 'lorem ipsum'};
  const commentB = {id: '5d707203b65083001e956f0a', comment: 'lorem ipsum'};
  const commentC = {id: '5d707203b65083001e956f0a', comment: 'lorem ipsum'};

  it('should handle ADD_REVIEW_COMMENT', () => {
    expect(reducer(INITIAL_STATE, {
      type: ADD_REVIEW_COMMENT,
      ...commentA,
    })).toEqual({
      view: ALL_REVIEWS_VIEW,
      reviews: [],
      reviewComments: Object.fromEntries([commentA].map(Object.values)),
    });
  });

  it('should handle ADD_REVIEW_COMMENT with new reviews', () => {
    expect(reducer({
          view: ALL_REVIEWS_VIEW,
          reviews: [],
          reviewComments: Object.fromEntries([commentA].map(Object.values)),
        }, {
          type: ADD_REVIEW_COMMENT,
          ...commentB,
        })
    ).toEqual({
      view: ALL_REVIEWS_VIEW,
      reviews: [],
      reviewComments: Object.fromEntries([commentA, commentB].map(Object.values)),
    });
  });

  it('should handle ADD_REVIEW_COMMENT with updated reviews', () => {
    expect(reducer({
          view: ALL_REVIEWS_VIEW,
          reviews: [],
          reviewComments: Object.fromEntries([commentA, commentB].map(Object.values)),
        }, {
          type: ADD_REVIEW_COMMENT,
          ...commentC, // updated reviews just replace existing entries in the object
        })
    ).toEqual({
      view: ALL_REVIEWS_VIEW,
      reviews: [],
      reviewComments: Object.fromEntries([commentA, commentC].map(Object.values)),
    });
  });
});
