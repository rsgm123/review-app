import {combineReducers} from 'redux'
import {
  CHANGE_VIEW,
  ALL_REVIEWS_VIEW,
  SET_REVIEWS, ADD_REVIEW_COMMENT,
} from './Actions'


function view(state = null, action) {
  if (action.type === CHANGE_VIEW) {
    return action.view;
  } else {
    return state
  }
}

function reviews(state = [], action) {
  if (action.type === SET_REVIEWS) {
    return action.reviews;
  } else {
    return state
  }
}

function reviewComments(state = {}, action) {
  if (action.type === ADD_REVIEW_COMMENT) {
    return {
      ...state,
      [action.id]: action.comment};
  } else {
    return state
  }
}


const reducer = combineReducers({
  view,
  reviews,
  reviewComments,
});

export default reducer
