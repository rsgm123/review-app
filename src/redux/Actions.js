import moment from "moment";

export const CHANGE_VIEW = 'CHANGE_VIEW';
export const ALL_REVIEWS_VIEW = null;

export const SET_REVIEWS = 'SET_REVIEWS';
export const ADD_REVIEW_COMMENT = 'ADD_REVIEW_COMMENT';


export function changeView(view) {
  return {type: CHANGE_VIEW, view}
}

export function setReviews(reviews) {
  return {type: SET_REVIEWS, reviews}
}

export function addReviewComment(id, content, author, published_at) {
  return {type: ADD_REVIEW_COMMENT, id, comment: {content, author, published_at}} // I'm only using snake case for consistency with reviews
}
