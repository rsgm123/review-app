import {
  CHANGE_VIEW,
  SET_REVIEWS,
  ADD_REVIEW_COMMENT,
  changeView,
  setReviews,
  addReviewComment,
} from './Actions'
import * as moment from "moment";


test('ChangeView action should reflect the new view', () => {
  const view = '5d707203b65083001e956f0a';
  const expected = {type: CHANGE_VIEW, view};
  const action = changeView(view);
  expect(action).toEqual(expected);
});

test('SetReviews action should reflect the new reviews', () => {
  const reviews = [{
    "id": "5d707203b65083001e956f0a",
    "author": "Weeks Duran",
    "place": "Big Johns Burgers",
    "published_at": "Thu Jun 27 1974 11:46:39 GMT-0500 (Central Daylight Time)",
    "rating": 5,
    "content": "Ipsum mollit anim pariatur eu qui velit Lorem ea enim excepteur ut fugiat fugiat esse. Incididunt consectetur deserunt pariatur magna sit dolore voluptate. Minim cupidatat fugiat magna quis consectetur esse id esse adipisicing anim velit. Cillum mollit et nisi ex occaecat labore enim nulla cupidatat. Occaecat Lorem officia est sit enim amet commodo sunt occaecat reprehenderit Lorem culpa. Aute anim ullamco voluptate incididunt incididunt excepteur in irure.\r\n"
  }];
  const expected = {type: SET_REVIEWS, reviews};
  const action = setReviews(reviews);
  expect(action).toEqual(expected);
});

test('AddReviewComment action should reflect the new reviews', () => {
  const review = {
    id: '5d707203b65083001e956f0a', comment: {
      content: 'lorem ipsum',
      author: 'Bob Bobbington',
      published_at: moment.now()
    }
  };
  const expected = {type: ADD_REVIEW_COMMENT, ...review};
  const action = addReviewComment(review.id, review.comment.content, review.comment.author, review.comment.published_at);
  expect(action).toEqual(expected);
});
