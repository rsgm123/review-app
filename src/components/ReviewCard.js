import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from "@material-ui/core/Card";
import StarIcon from '@material-ui/icons/Star';
import ForumIcon from '@material-ui/icons/Forum';
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import moment from 'moment';
import {connect} from "react-redux";
import {changeView} from "../redux/Actions";


const useStyles = makeStyles({
  card: {
    margin: '50px',
  },
  listCard: {
    margin: '50px',
    cursor: 'pointer',
  },
  cardFooter: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between'
  }
});

export function ReviewCard({review, reviewComment, listCard, goToReview}) {
  const classes = useStyles();
  const cardClass = listCard ? classes.listCard : classes.card;

  function cardAction() {
    if (listCard) {
      goToReview(review.id)
    }
  }

  const content = listCard && review.content.length > 200 ? review.content.substring(0, 197) + '...' : review.content;
  const comments = reviewComment ? <ForumIcon color="primary"/> : <div/>;

  // moment doesn't recognise the format, js date seems to work in firefox
  const date = moment(review.published_at).format('MM/DD/Y');

  return (
      <Card className={cardClass} onClick={cardAction}>
        <CardContent>
          <Typography variant="h4">{review.place}</Typography>

          <div>{Array(review.rating).fill().map((_, id) =>
              <StarIcon data-testid="star" color="secondary" key={id}/>)}</div>

          <Typography variant="body1">{content}</Typography>

          <div className={classes.cardFooter}>
            <Typography variant="caption">{review.author}</Typography>
            <Typography variant="caption">{date}</Typography>
            {comments}
          </div>
        </CardContent>
      </Card>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    reviewComment: state.reviewComments[ownProps.review.id],
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    goToReview: (reviewId) => dispatch(changeView(reviewId))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewCard);
