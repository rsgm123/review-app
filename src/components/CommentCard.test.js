import React from 'react';
import {render} from '@testing-library/react';
import {ReviewCard} from "./ReviewCard";
import moment from "moment";
import {CommentCard} from "./CommentCard";


describe('ReviewCard component', () => {
  it('should display the given review', () => {
    const reviewId = '5d707203b65083001e956f0a';
    const comment = {
      content: 'review content',
      author: 'review author',
      published_at: moment.now(),
    };

    const date = moment(comment.published_at).format('MM/DD/Y');

    const {getByText} = render(<CommentCard reviewId={reviewId} reviewComment={comment}/>);
    expect(getByText(comment.content)).toBeInTheDocument();
    expect(getByText(comment.author)).toBeInTheDocument();
    expect(getByText(date)).toBeInTheDocument();
  })
});
