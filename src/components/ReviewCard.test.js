import React from 'react';
import {render} from '@testing-library/react';
import {ReviewCard} from "./ReviewCard";
import moment from "moment";

const review = {
  "id": "5d707203b65083001e956f0a",
  "author": "Weeks Duran",
  "place": "Big Johns Burgers",
  "published_at": "Thu Jun 27 1974 11:46:39 GMT-0500 (Central Daylight Time)",
  "rating": 5,
  "content": "Ipsum mollit anim pariatur eu qui velit Lorem ea enim excepteur ut fugiat fugiat esse. Incididunt consectetur deserunt pariatur magna sit dolore voluptate. Minim cupidatat fugiat magna quis consectetur esse id esse adipisicing anim velit. Cillum mollit et nisi ex occaecat labore enim nulla cupidatat. Occaecat Lorem officia est sit enim amet commodo sunt occaecat reprehenderit Lorem culpa. Aute anim ullamco voluptate incididunt incididunt excepteur in irure.\r\n"
};

describe('ReviewCard component', () => {
  it('should display the given review', () => {
    const date = moment(review.published_at).format('MM/DD/Y');

    const {getByText, getAllByTestId} = render(<ReviewCard review={review}/>);
    expect(getByText(review.place)).toBeInTheDocument();
    expect(getByText(review.author)).toBeInTheDocument();
    // expect(getByText(review.content)).toBeInTheDocument(); // todo: fix this assertion
    expect(getByText(date)).toBeInTheDocument();
    expect(getAllByTestId('star').length).toBe(review.rating);
  });
  it('should truncate long reviews', () => {
    const content = review.content.length <= 200 ? review.content : review.content.substring(0, 197) + '...';

    const {getByText, getAllByTestId} = render(<ReviewCard review={review} listCard/>);
    expect(getByText(content)).toBeInTheDocument();
  });
});
