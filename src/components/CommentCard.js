import React from 'react';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import moment from 'moment';
import {connect} from "react-redux";
import TextField from "@material-ui/core/TextField";
import {addReviewComment, ALL_REVIEWS_VIEW, changeView} from "../redux/Actions";
import {withStyles} from "@material-ui/styles";
import Button from "@material-ui/core/Button";


const styles = {
  card: {
    margin: '50px',
  },
  input: {
    margin: '10px 0',
  },
  actions: {
    '& button': {
      marginRight: '10px',
    }
  }
};

class CommentCardComponent extends React.Component {
  constructor(props) {
    super(props);

    const {reviewId, reviewComment} = this.props;

    this.state = {
      editing: !reviewComment.author, // start editing if there is no reviewComment
      reviewComment: {
        id: reviewId,
        content: reviewComment.content || '',
        author: reviewComment.author || ''
      }
    };

    this.toggleEditing = this.toggleEditing.bind(this);
    this.updateComment = this.updateComment.bind(this);
    this.save = this.save.bind(this);
  }

  toggleEditing() {
    this.setState({...this.state, editing: !this.state.editing})
  }

  updateComment(event) {
    this.setState({
      ...this.state,
      reviewComment: {
        ...this.state.reviewComment,
        [event.target.id]: event.target.value,
      },
    })
  }

  save() {
    const {addComment} = this.props;
    addComment(this.state.reviewComment)
    this.toggleEditing()
  }

  render() {
    const {classes, resetView} = this.props;
    const date = this.props.reviewComment.published_at ? moment(this.props.reviewComment.published_at).format('MM/DD/Y') : '';

    let content;
    if (this.state.editing) {
      content = (
          <CardContent>
            <form>
              <TextField className={classes.input}
                         id="content"
                         label="Comment"
                         fullWidth
                         value={this.state.reviewComment.content}
                         error={!this.state.reviewComment.content}
                         onChange={this.updateComment}/>
              <span>
                <TextField className={classes.input}
                           id="author"
                           label="Author"
                           value={this.state.reviewComment.author}
                           error={!this.state.reviewComment.author}
                           onChange={this.updateComment}/>
                <Typography variant="caption">{date}</Typography>
              </span>
              <div className={classes.actions}>
                <Button type="button"
                        variant="contained"
                        disabled={!this.state.reviewComment.content || !this.state.reviewComment.author}
                        onClick={this.save}>
                  save
                </Button>
                <Button type="button" variant="contained" color="primary" onClick={resetView}>back</Button>
              </div>
            </form>
          </CardContent>
      )
    } else {
      content = (
          <CardContent>
            <Typography variant="h6">{this.props.reviewComment.content}</Typography>
            <span>
              <Typography variant="caption">{this.props.reviewComment.author}</Typography>
              <Typography variant="caption">{date}</Typography>
            </span>
            <div className={classes.actions}>
              <Button variant="contained" onClick={this.toggleEditing}>update</Button>
              <Button variant="contained" color="primary" onClick={resetView}>back</Button>
            </div>
          </CardContent>
      )
    }

    return <Card className={classes.card}>{content}</Card>
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    reviewComment: state.reviewComments[ownProps.reviewId] || {},
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    addComment: ({id, content, author}) => dispatch(addReviewComment(id, content, author, moment.now())),
    resetView: () => dispatch(changeView(ALL_REVIEWS_VIEW))
  }
};

export const CommentCard = withStyles(styles)(CommentCardComponent); // this is kind of gross
export default connect(mapStateToProps, mapDispatchToProps)(CommentCard); // this is kind of gross
