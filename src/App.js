import React from 'react';
import Reviews from './views/Reviews';
import ReviewDetail from './views/ReviewDetail';
import {connect} from 'react-redux';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {grey} from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => {
  return {
    root: {
      minHeight: '100vh',
      background: grey[500]
    },
    title: {
      padding: theme.spacing(2),
    }
  }
});

export function App({view}) {
  let viewComponent = view ? <ReviewDetail/> : <Reviews/>;

  const classes = useStyles();
  return <div className={classes.root}>
    <AppBar position="static" role="toolbar">
      <Toolbar>
        <Typography variant="h3" className={classes.title}>
          Reviews
        </Typography>
      </Toolbar>
    </AppBar>

    {viewComponent}
  </div>;
}

const mapStateToProps = (state) => {
  return {
    view: state.view
  }
};

export default connect(mapStateToProps)(App);
