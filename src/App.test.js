import React from 'react';
import {render} from '@testing-library/react';
import {App} from './App';
import Reviews from "./views/Reviews";
import ReviewDetail from "./views/ReviewDetail";

jest.mock('./views/Reviews');
jest.mock('./views/ReviewDetail');
// Reviews.mockImplementation(({}) => <div>reviews component</div>);
// ReviewDetail.mockImplementation(({view}) => <div>
//   <div>review detail</div>
//   <div>{view}</div>
// </div>);

// todo: get mocking components working correctly
describe('Root App', () => {
  it('should have a toolbar with a title', () => {
    const {getByText, getByRole} = render(<App/>);
    expect(getByRole('toolbar')).toBeInTheDocument();
    expect(getByText('Reviews')).toBeInTheDocument();
  });
  // it('should have a reviews component when no view given', () => {
  //   const {getByText} = render(<App/>);
  //   expect(getByText('reviews component')).toBeInTheDocument();
  // });
  // it('should have a review details component for the given review', () => {
  //   const expected = '5d707203b65083001e956f0a';
  //   const {getByText} = render(<App view={expected}/>);
  //   expect(getByText('review detail')).toBeInTheDocument();
  //   expect(getByText(expected)).toBeInTheDocument();
  // });
});
